<?php

// Course Plugin
$pluginSignature = \TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
    'drk_courseview',
    'Courseview',
    'LLL:EXT:drk_courseview/Resources/Private/Language/locallang_be.xlf:tt_content.course_plugin.title',
    'EXT:drk_courseview/Resources/Public/Icons/drk-logo-icon.svg',
    'DRK Kurstermine'
);

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPiFlexFormValue(
    '*',
    // FlexForm configuration schema file
    'FILE:EXT:drk_courseview/Configuration/FlexForms/courseview.xml',
    // ctype
    'drkcourseview_courseview'
);

$GLOBALS['TCA']['tt_content']['types'][$pluginSignature] = array_replace_recursive(
    $GLOBALS['TCA']['tt_content']['types'][$pluginSignature],
    [
        'showitem' => '
            --div--;General,
            --palette--;General;general,
            --palette--;Headers;headers,
            --div--;Einstellungen,
            pi_flexform'
    ]
);

// course-ls Plugin
$pluginSignature = \TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
    'drk_courseview',
    'Courseview-ls',
    'LLL:EXT:drk_courseview/Resources/Private/Language/locallang_be.xlf:tt_content.course_plugin.title-ls',
    'EXT:drk_courseview/Resources/Public/Icons/drk-logo-icon.svg',
    'DRK Kurstermine'
);

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPiFlexFormValue(
    '*',
    // FlexForm configuration schema file
    'FILE:EXT:drk_courseview/Configuration/FlexForms/courseview-ls.xml',
    // ctype
    'drkcourseview_courseview-ls'
);

$GLOBALS['TCA']['tt_content']['types'][$pluginSignature] = array_replace_recursive(
    $GLOBALS['TCA']['tt_content']['types'][$pluginSignature],
    [
        'showitem' => '
            --div--;General,
            --palette--;General;general,
            --palette--;Headers;headers,
            --div--;Einstellungen,
            pi_flexform,
            --div--;Zugriff,
            --palette--;Access;access'
    ]
);

// course-fbw Plugin

$pluginSignature = \TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
    'drk_courseview',
    'Courseview-fbw',
    'LLL:EXT:drk_courseview/Resources/Private/Language/locallang_be.xlf:tt_content.course_plugin.title-fbw',
    'EXT:drk_courseview/Resources/Public/Icons/drk-logo-icon.svg',
    'DRK Kurstermine'
);

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPiFlexFormValue(
    '*',
    // FlexForm configuration schema file
    'FILE:EXT:drk_courseview/Configuration/FlexForms/courseview-fbw.xml',
    // ctype
    'drkcourseview_courseview-fbw'
);

$GLOBALS['TCA']['tt_content']['types'][$pluginSignature] = array_replace_recursive(
    $GLOBALS['TCA']['tt_content']['types'][$pluginSignature],
    [
        'showitem' => '
            --div--;General,
            --palette--;General;general,
            --palette--;Headers;headers,
            --div--;Einstellungen,
            pi_flexform,
            --div--;Zugriff,
            --palette--;Access;access'
    ]
);
