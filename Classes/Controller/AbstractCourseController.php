<?php

namespace DRK\DrkCourseview\Controller;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2023 André Gyöngyösi <a.gyoengyyoesi@drkserivce.de>
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use DRK\DrkGeneral\Controller\AbstractDrkController;
use DRK\DrkCourseview\Domain\Repository\CourseRepository;
use Exception;
use Psr\Http\Message\ResponseInterface;
use TYPO3\CMS\Core\Page\AssetCollector;


/**
 * Class AbstractCourseController
 * @package DRK\DrkCourseview\Controller
 */
abstract class AbstractCourseController extends AbstractDrkController
{
    /**
     * @var CourseRepository
     */
    protected $courseRepository;

    /**
     * @var string
     */
    protected $courseTyp = 'K';

    /**
     * @param AssetCollector $assetCollector
     */
    public function __construct(
        protected readonly AssetCollector $assetCollector
    ) {
    }

    /**
     * @param CourseRepository $courseRepository
     */
    public function injectCourseRepository(CourseRepository $courseRepository)
    {
        $this->courseRepository = $courseRepository;
    }

    /**
     * index view
     * @throws Exception
     */
    public function indexAction(): ResponseInterface
    {
        if (!empty($this->settings['preselection_organisation'])) {
            $orgCode = $this->settings['preselection_organisation'];
        } elseif (!empty($this->settings['man_preselection_organisation'])) {
            $orgCode = $this->settings['man_preselection_organisation'];
        } else {
            $orgCode = '';
        }

        $courseType = $this->settings['preselection_course'] ??= '9999';

        $courseLabel = $this->settings['preselection_course_label'] ??= null;

        $response = $this->courseRepository->getCoursebyOrgCode($orgCode, $courseType, $courseLabel);

        $courseList = $this->courseRepository->getCourseTypeList($this->courseTyp);

        $preselection = [];
        foreach ($courseList as $course) {
            if ((int)$course['CourseType'] === (int)$courseType) {
                $preselection = $course;
                break;
            }
        }

        if (empty($preselection)) {
            throw new Exception('FEHLER: Es wurde keine Kurstyp ausgewählt!');
        }

        $aCourses = [];

        foreach ($response as $aCourse) {
            // OpenSlots = -1 should be shown
            if ($aCourse['OpenSlots'] == 0 && $this->settings['show_zero_OpenSlots'] == 0) {
                //do nothing
            } else {
                $aCourses[] = $aCourse;
            }
        }
        $this->view->assignMultiple([
            'courseType' => $courseType,
            'courses' => $aCourses,
            'preselectionCourse' => $preselection,
        ]);

        return $this->htmlResponse();
    }
}
