<?php

namespace DRK\DrkCourseview\Domain\Repository;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2023 André Gyöngyösi <a.gyoengyyoesi@drkserivce.de>
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use DRK\DrkGeneral\Domain\Repository\AbstractDrkRepository;
use DRK\DrkGeneral\Utilities\Utility;
use TYPO3\CMS\Core\Cache\Exception\NoSuchCacheException;

class CourseRepository extends AbstractDrkRepository
{
    /**
     * @param $orgCode
     * @param $courseType
     *
     * @return array
     * @throws NoSuchCacheException
     */
    public function getCoursebyOrgCode($orgCode, $courseType, $courseLabel = null): array
    {
        $cacheIdentifier = sha1(json_encode(['drk_courseview|getCoursebyOrgCode', $orgCode, $courseType, $courseLabel]));
        $result = $this->getCache()->get($cacheIdentifier);

        if (($result !== null) && ($result !== false)) {
            return $result;
        }

        $courses = Utility::convertObjectToArray(
            $this->getCourseClient()->getCoursebyOrgCode($orgCode, $courseType, $courseLabel)->Courses
        );

        if (empty($courses)) {
            return [];
        }

        Utility::sortCourseArrayByDate($courses);

        foreach ($courses as &$course) {
            $course['Date'] = Utility::generateCourseDates($course);
        }

        $this->getCache()->set($cacheIdentifier, $courses, [], $this->short_cache);

        return $courses;
    }

    /**
     * @param string $sCourseType
     * @param bool $sortAlphabetically
     *
     * @return array
     * @throws NoSuchCacheException
     */
    public function getCourseTypeList($sCourseType = 'K', $sortAlphabetically = false): array
    {
        $cacheIdentifier = sha1(json_encode(['drk_courseview|getCourseTypeList', $sCourseType, $sortAlphabetically]));
        $result = $this->getCache()->get($cacheIdentifier);

        if (($result !== null) && ($result !== false)) {
            return $result;
        }

        switch ($sCourseType) {
            case 'L':
                $courses = Utility::convertObjectToArray(
                    $this->getCourseClient()->getLsCourseTypeList()->CourseTypes
                );
                break;

            case 'F':
                $courses = Utility::convertObjectToArray(
                    $this->getCourseClient()->getFbwCourseTypeList()->CourseTypes
                );
                break;

            default:
                $courses = Utility::convertObjectToArray(
                    $this->getCourseClient()->getCourseTypeList()->CourseTypes
                );

        }

        if (empty($courses)) {
            return [];
        }

        if ($sortAlphabetically == true) {
            usort($courses, function ($a, $b) {
                return strcmp($a["CourseTypeDescription"], $b["CourseTypeDescription"]);
            });
        }

        $this->getCache()->set($cacheIdentifier, $courses, [], $this->heavy_cache);

        return $courses;
    }

    /**
     * @param string $sCourseType
     * @param bool $sortAlphabetically
     *
     * @return array
     * @throws NoSuchCacheException
     */
    public function getCourseLabelList($sOrgCode = '2501', $iCourseType = 0, $sortAlphabetically = true): array
    {
        $cacheIdentifier = sha1(json_encode(['drk_courseview|getCourseLabels', $sOrgCode, $iCourseType]));
        $result = $this->getCache()->get($cacheIdentifier);

        if (($result !== null) && ($result !== false)) {
            return $result;
        }
        $courseLabels = Utility::convertObjectToArray(
            $this->getCourseClient()->getCourseLabels($sOrgCode, $iCourseType)->CourseTypeLabels
        );

        if (empty($courseLabels)) {
            return [];
        }
        if ($sortAlphabetically == true) {
            usort($courseLabels, function ($a, $b) {
                return strcmp($a["CourseTypeLabel"], $b["CourseTypeLabel"]);
            });
        }

        $this->getCache()->set($cacheIdentifier, $courseLabels, [], $this->heavy_cache);

        return $courseLabels;
    }

    /**
     * @param string $orgType
     * @param null $sortingBy
     * @param null $orderBy
     *
     * @return array
     * @throws NoSuchCacheException
     */
    public function getOrganisationList($orgType = 'K', $sortingBy = null, $orderBy = null): array
    {
        $cacheIdentifier = sha1(json_encode(['drk_courseview|getOrganisationList', $orgType, $sortingBy, $orderBy]));
        $result = $this->getCache()->get($cacheIdentifier);

        if (($result !== null) && ($result !== false)) {
            return $result;
        }

        $organisations = Utility::convertObjectToArray(
            $this->getDldbClient()->getOrganisationList($orgType, $sortingBy, $orderBy)
        );

        if (empty($organisations)) {
            return [];
        }

        $this->getCache()->set($cacheIdentifier, $organisations, [], $this->heavy_cache);

        return $organisations;
    }

    /**
     * ItemsProcFunc for Courseview->settings.flexforms.preselection_course
     *
     * @param array $parameters
     *
     * @return array
     * @throws NoSuchCacheException
     */
    public function addCourseListItems(array $parameters): array
    {
        // check courseType from itemsProcFunc_params
        if (isset($parameters['config']['itemsProcFunc_params']['courseType'])) {
            $sCourseTyp = $parameters['config']['itemsProcFunc_params']['courseType'];
        } else {
            $sCourseTyp = 'K';
        }
        // check sortAlphabetically from itemsProcFunc_params
        if (isset($parameters['config']['itemsProcFunc_params']['sortAlphabetically'])) {
            $sortAlphabetically = $parameters['config']['itemsProcFunc_params']['sortAlphabetically'];
        } else {
            $sortAlphabetically = false;
        }

        $this->initSettingsForListItems($parameters);
        foreach ($this->getCourseTypeList($sCourseTyp, $sortAlphabetically) as $course) {
            $parameters['items'][] = [
                $course['CourseTypeDescription'],
                $course['CourseType'],
                '',
            ];
        }

        return $parameters;
    }

    /**
     * ItemsProcFunc for Courseview->settings.flexforms.preselection_course_label
     *
     * @param array $parameters
     *
     * @return array
     * @throws NoSuchCacheException
     */
    public function addCourseLabelItems(array $parameters): array
    {
        // check sortAlphabetically from itemsProcFunc_params
        if (isset($parameters['config']['itemsProcFunc_params']['sortAlphabetically'])) {
            $sortAlphabetically = $parameters['config']['itemsProcFunc_params']['sortAlphabetically'];
        } else {
            $sortAlphabetically = false;
        }

        // check orgCode from itemsProcFunc_params
        if (isset($parameters['row']['settings.flexforms.preselection_organisation'][0]) &&
            !empty($parameters['row']['settings.flexforms.preselection_organisation'][0])
        )
        {
            $sOrgCode = $parameters['row']['settings.flexforms.preselection_organisation'][0];
        }
        elseif (
            isset($parameters['row']['settings.flexforms.man_preselection_organisation']) &&
            !empty($parameters['row']['settings.flexforms.man_preselection_organisation'])
        )
        {
            $sOrgCode = $parameters['row']['settings.flexforms.man_preselection_organisation'];
        }
        else {
            $sOrgCode = "2501";
        }

        // check preselectionCourse from itemsProcFunc_params
        if (isset($parameters['row']['settings.flexforms.preselection_course'][0]) &&
            !empty($parameters['row']['settings.flexforms.preselection_course'])
        )
        {
            $iCourseType = $parameters['row']['settings.flexforms.preselection_course'][0];
        } else {
            $iCourseType = "75";
        }

        $this->initSettingsForListItems($parameters);
        foreach ($this->getCourseLabelList($sOrgCode, $iCourseType, $sortAlphabetically) as $courselabels) {
            $parameters['items'][] = [
                $courselabels['CourseTypeLabel'],
                $courselabels['CourseTypeLabelId'],
                '',
            ];
        }
        return $parameters;
    }

    /**
     * ItemsProcFunc for Courseview->settings.flexforms.preselection_organisation
     *
     * @param array $parameters
     *
     * @return array
     * @throws NoSuchCacheException
     */
    public function addOrganisationListItems(array $parameters): array
    {
        $this->initSettingsForListItems($parameters);

        // get LV
        foreach ($this->getOrganisationList('L') as $organisation) {
            $parameters['items'][] = [
                "LV " . $organisation['orgName'],
                $organisation['orgID'],
                '',
            ];
        }
        // get KV
        foreach ($this->getOrganisationList() as $organisation) {
            $parameters['items'][] = [
                "KV " . $organisation['orgName'],
                $organisation['orgID'],
                '',
            ];
        }

        return $parameters;
    }
}
