<?php

declare(strict_types=1);

namespace DRK\DrkCourseview\Updates;

use Doctrine\DBAL\Exception;
use DRK\DrkContactform\Updates\AbstractRecordUpdater;
use DRK\DrkContactform\Updates\InvalidArgumentException;
use TYPO3\CMS\Core\Database\Connection;
use TYPO3\CMS\Core\Database\ConnectionPool;
use TYPO3\CMS\Core\Database\Query\QueryBuilder;
use TYPO3\CMS\Core\Database\Query\Restriction\DeletedRestriction;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Install\Attribute\UpgradeWizard;
use TYPO3\CMS\Install\Updates\UpgradeWizardInterface;

#[UpgradeWizard('drk_courseview_courseview_UpdateCtype')]
class UpdateCtypeCourseView extends AbstractRecordUpdater implements UpgradeWizardInterface
{
    protected string $pluginSignature = 'drkcourseview_courseview';

    /**
     * Return the speaking name of this wizard
     */
    public function getTitle(): string
    {
        return 'Migrates '.$this->pluginSignature.' to CType';
    }

    /**
     * Return the description for this wizard
     */
    public function getDescription(): string
    {
        return 'Migrates plugin from plugin list to CType.';
    }

    /**
     * Execute the update
     *
     * Called when a wizard reports that an update is necessary
     * @throws Exception
     */
    public function executeUpdate(): bool
    {
        /** @var Connection $connection */
        $connection = GeneralUtility::makeInstance(ConnectionPool::class)->getConnectionForTable('tt_content');
        $queryBuilder = $connection->createQueryBuilder();
        $queryBuilder->getRestrictions()->removeAll()->add(GeneralUtility::makeInstance(DeletedRestriction::class));

        $results = $queryBuilder
            ->select('*')
            ->from('tt_content')
            ->where(
                $queryBuilder->expr()->like('list_type', $queryBuilder->createNamedParameter($this->pluginSignature))
            )
            ->executeQuery()
            ->fetchAllAssociative();

        foreach ($results as $result) {
            $connection->update(
                'tt_content',
                [
                    'CType' => $this->pluginSignature,
                    'list_type' => ''
                ],
                [
                    'uid' => $result['uid']
                ]
            );
        }
        return true;
    }

    /**
     * Check if there are record within database table with an empty "slug" field.
     *
     * @return bool
     * @throws InvalidArgumentException
     * @throws Exception
     */
    protected function checkIfWizardIsRequired(): bool
    {
        $connectionPool = GeneralUtility::makeInstance(ConnectionPool::class);
        /** @var QueryBuilder $queryBuilder */
        $queryBuilder = $connectionPool->getQueryBuilderForTable('tt_content');
        $queryBuilder->getRestrictions()->removeAll()->add(GeneralUtility::makeInstance(DeletedRestriction::class));

        $numberOfEntries = $queryBuilder
            ->count('uid')
            ->from('tt_content')
            ->where(
                $queryBuilder->expr()->like('list_type', $queryBuilder->createNamedParameter($this->pluginSignature))
            )
            ->executeQuery()
            ->fetchOne();
        return $numberOfEntries > 0;
    }

}
