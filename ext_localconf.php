<?php
if (!defined('TYPO3')) {
    die('Access denied.');
}

// course Plugin
\TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
    'drk_courseview',
    'Courseview',
    [
        \DRK\DrkCourseview\Controller\CourseController::class => 'index',

    ],
    // non-cacheable actions
    [
        \DRK\DrkCourseview\Controller\CourseController::class => 'index',

    ],
    \TYPO3\CMS\Extbase\Utility\ExtensionUtility::PLUGIN_TYPE_CONTENT_ELEMENT
);

// course-ls Plugin
\TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
    'drk_courseview',
    'Courseview-ls',
    [
        \DRK\DrkCourseview\Controller\CourseLsController::class => 'index',

    ],
    // non-cacheable actions
    [
        \DRK\DrkCourseview\Controller\CourseLsController::class => 'index',

    ],
    \TYPO3\CMS\Extbase\Utility\ExtensionUtility::PLUGIN_TYPE_CONTENT_ELEMENT
);

// course-fbw Plugin
\TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
    'drk_courseview',
    'Courseview-fbw',
    [
        \DRK\DrkCourseview\Controller\CourseFbwController::class => 'index',

    ],
    // non-cacheable actions
    [
        \DRK\DrkCourseview\Controller\CourseFbwController::class => 'index',

    ],
    \TYPO3\CMS\Extbase\Utility\ExtensionUtility::PLUGIN_TYPE_CONTENT_ELEMENT
);
